package digital.amaranth.mc.blocksrealism;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import digital.amaranth.mc.quickblocklib.BlockGroups;
import digital.amaranth.mc.quickblocklib.FaceGroups;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import org.bukkit.entity.Player;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World.Environment;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;

import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntitySpawnEvent;

public class StructuralIntegrity implements Listener { 
    private final int PATH_MAX_SIDESTEPS;
   
    private final boolean creativeDestroyBlocks;
    private final boolean buoyancyEnabled;
    
    public StructuralIntegrity () {
        this.PATH_MAX_SIDESTEPS = 20;           // number of horizontal steps to allow per pathing event
        
        this.creativeDestroyBlocks = true;      // whether to destroy blocks in creative mode, rather than breaking them
        this.buoyancyEnabled = true;            // whether to allow liquids to support weight
    }
    
    private void placeFallingBlock (Block block) {
        Location location = block.getLocation();
        
        location.setX(location.getBlockX()+0.5);
        location.setZ(location.getBlockZ()+0.5);
                
        block.getWorld().spawnFallingBlock(location, block.getBlockData());
    }
    
    private void makeBlockFall (Block block) {
        if (block.getType().isSolid()) {
            placeFallingBlock(block);
            block.setType(Material.AIR);
            checkBlockNeighbors(block);
        }
    }
    
    private void destroyBlock (Block block) {
        block.setType(Material.AIR);
        checkBlockNeighbors(block);
    }
    
    private void breakBlock (Block block) {
        block.breakNaturally();
        checkBlockNeighbors(block);
    }
    
    private void breakBlock (Block block, Player player) {
        block.breakNaturally(player.getItemInUse());
        checkBlockNeighbors(block);
    }
    
    private void checkBlockNeighbors (Block block) {
        for (BlockFace face : FaceGroups.allStraightFacesButUp) {
            Block blockAtFace = block.getRelative(face);
            
            if (blockAtFace.getType().isSolid()) {
                checkBlock(blockAtFace);
            }
        }
        
        // here we make sure that the game's built-in falling blocks don't circumvent our physics checks
        block = block.getRelative(BlockFace.UP);
        while (BlockGroups.isLoose(block)) {
            block = block.getRelative(BlockFace.UP);
        }
        
        if (block.getType().isSolid()) {
            checkBlock(block);
        }
    }
    
    private boolean blockIsSupportive (Block block) {
        Material blockType = block.getType();
        return blockType.isSolid() || (buoyancyEnabled && (blockType == Material.WATER || blockType == Material.LAVA));
    }
    
    // quick and cheap search for a solid path to bedrock, simulating support against gravity
    private boolean checkBlock (Block block) { 
        Block branchBlock = block;
        Block previousBlock = block;
        List<Block> branchBlocks = new ArrayList();
        
        for (int stepsTaken = 0; stepsTaken <= PATH_MAX_SIDESTEPS; ++stepsTaken) {
            Block blockDown = block.getRelative(BlockFace.DOWN);
            
            while (!blockDown.equals(previousBlock) && !blockDown.equals(branchBlock) && blockIsSupportive(blockDown)) {
                block = blockDown;
                blockDown = block.getRelative(BlockFace.DOWN);
            }
            
            if (blockDown.getType() == Material.BEDROCK) {
                return true;
            } else {
                for (BlockFace face : FaceGroups.allStraightFacesButDown) {
                    Block pathBlock = block.getRelative(face);
                    
                    if (!pathBlock.equals(previousBlock) && !pathBlock.equals(branchBlock) && blockIsSupportive(pathBlock)) {
                        if(face != BlockFace.UP) {
                            Block pathBlockNextDown = pathBlock.getRelative(BlockFace.DOWN);
                            
                            while (blockIsSupportive(pathBlockNextDown)) {
                                pathBlock = pathBlockNextDown;
                                pathBlockNextDown = pathBlock.getRelative(BlockFace.DOWN);
                            }
                            
                            if (pathBlock.getType() == Material.BEDROCK) {
                                return true;
                            }
                        }
                        
                        branchBlocks.add(pathBlock);
                    }
                }
                
                if (!branchBlocks.isEmpty()) {
                    Collections.sort(branchBlocks, (Block b1, Block b2) -> b1.getY() - b2.getY());
                    
                    previousBlock = block;
                    block = branchBlocks.get(0);
                    branchBlocks.remove(0);
                    
                    if (!branchBlocks.isEmpty()) {
                        branchBlock = previousBlock;
                    }
                } else {
                    break;
                }
            }
        }
        
        makeBlockFall(block);
                
        return false;
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockPlaceEvent (BlockPlaceEvent event) {
        if (event.getBlock().getWorld().getEnvironment() == Environment.NORMAL) { // don't modify end/nether physics
            checkBlock(event.getBlockPlaced());
        }
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockBreakEvent (BlockBreakEvent event) {
        Block block = event.getBlock();
        
        if (event.getBlock().getWorld().getEnvironment() == Environment.NORMAL) { // don't modify end/nether physics
            if (block.getType().isSolid()) {
                if (creativeDestroyBlocks && event.getPlayer().getGameMode() == GameMode.CREATIVE) {
                    event.setCancelled(true);
                    
                    destroyBlock(block);
                } else {
                    breakBlock(block, event.getPlayer());
                }
            }
        }
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onEntitySpawnEvent (EntitySpawnEvent event) {
        if (event.getEntityType() == EntityType.PRIMED_TNT) { // structures react when TNT is primed 
            Block block = event.getLocation().getBlock();
            if (block.getWorld().getEnvironment() == Environment.NORMAL) { // don't modify end/nether physics
                destroyBlock(block);
            }
        }
    }
    
    @EventHandler 
    public void onBlockBurnEvent (BlockBurnEvent event) {
        if (event.getBlock().getWorld().getEnvironment() == Environment.NORMAL) { // don't modify end/nether physics
            event.setCancelled(true);
            destroyBlock(event.getBlock());
        }
    }

    @EventHandler (ignoreCancelled = true)
    public void onLeavesDecayEvent (LeavesDecayEvent event) {
        if (event.getBlock().getWorld().getEnvironment() == Environment.NORMAL) { // don't modify end/nether physics
            event.setCancelled(true);
            destroyBlock(event.getBlock());
        }
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onEntityExplodeEvent (EntityExplodeEvent event) {
        if (event.getEntity().getWorld().getEnvironment() == Environment.NORMAL) { // don't modify end/nether physics
            event.blockList().forEach(blockInList -> {
                if (blockInList.getType() != Material.TNT) {
                    breakBlock(blockInList);
                }
            });
        }
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockExplodeEvent (BlockExplodeEvent event) {
        if (event.getBlock().getWorld().getEnvironment() == Environment.NORMAL){ // don't modify end/nether physics
            event.blockList().forEach(blockInList -> {
                if (blockInList.getType() != Material.TNT) {
                    breakBlock(blockInList);
                }
            });
        }
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockPistonRetractEvent (BlockPistonRetractEvent event) {
        if (event.getBlock().getWorld().getEnvironment() == Environment.NORMAL) { // don't modify end/nether physics
            event.getBlocks().forEach(blockInList -> {
                breakBlock(blockInList.getRelative(BlockFace.UP));
            });
        }
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockPistonExtendEvent (BlockPistonExtendEvent event) {
        if (event.getBlock().getWorld().getEnvironment() == Environment.NORMAL) { // don't modify end/nether physics
            event.getBlocks().forEach(blockInList -> {
                breakBlock(blockInList.getRelative(BlockFace.UP));
            });
        }
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockFadeEvent (BlockFadeEvent event) {
        Block block = event.getBlock();
        if (block.getWorld().getEnvironment() == Environment.NORMAL) { // don't modify end/nether physics
            Material newMaterial = event.getNewState().getBlock().getType();
            if (!newMaterial.isSolid()) {
                event.setCancelled(true);
                destroyBlock(block);
            }
        }
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockGrowEvent (BlockGrowEvent event) {
        Block block = event.getNewState().getBlock();
        
        if (block.getWorld().getEnvironment() == Environment.NORMAL) { // don't modify end/nether physics
            checkBlockNeighbors(block);
        }
    }
}
