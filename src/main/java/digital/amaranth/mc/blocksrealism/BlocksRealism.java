package digital.amaranth.mc.blocksrealism;

import digital.amaranth.mc.quickblocklib.QuickBlockLib;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;

import org.bukkit.plugin.java.JavaPlugin;

public class BlocksRealism extends JavaPlugin {
    public static BlocksRealism getInstance() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("BlocksRealism");
        if (plugin == null || !(plugin instanceof BlocksRealism)) {
            throw new RuntimeException("'BlocksRealism' not found.");
        }
        
        return ((BlocksRealism) plugin);
    }
    
    @Override
    public void onEnable() {
        QuickBlockLib.setPlugin(this);
        
        getServer().getPluginManager().registerEvents(new StructuralIntegrity(), this);
    }
    
    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }
}