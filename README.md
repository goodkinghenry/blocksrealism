This plugin, using the Spigot API 1.18, searches for a path from any updated block to the bedrock while in the overworld, causing the block to fall if this fails. As a result, block gravity is simulated.

BlocksRealism depends upon a small library I created, called QuickBlockLib, which is available here:
https://www.spigotmc.org/resources/quickblocklib.98623/